let createError = require('http-errors')
let express = require('express')
let path = require('path')
let logger = require('morgan')
let bodyParser = require('body-parser')

let app = express()

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({'extended':'false'}))

require('./routes/route')(app)

app.use(express.static('dist'))

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'dist', 'index.html'))
})

// Catch 400 error
app.use(function(req, res, next) {
  next(createError(404))
})

const PORT = process.env.PORT || 3000;
app.listen(PORT);
const _ = require('lodash');
let Solver = require('../services/Solver');

module.exports = app => {

    app.post('/puzzle', (req, res) => {
        const { day, puzzle, part } = req.body
        
        let data = {
            id: day,
            puzzle: puzzle,
            part: part
        }
        Solver.solvePuzzle(
            data,
            function(result) {
                let response = result
                res.status(200).send({
                    result: response
                })
            }
        )
    })
}

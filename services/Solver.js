let Solver = {

    // Select the needed puzzle solver function
    solvePuzzle: function(data, callback) {
        let id = parseInt(data.id)

        switch(id) {
            case 1:
                return this.firstPuzzle(data, callback)
                break
            case 2:
                return this.secondPuzzle(data, callback)
                break
            case 4:
                return this.fourthPuzzle(data, callback)
                break
            default:
                return callback('error')
                break
        }
    },

    // Solve "Inverse Captcha"
    firstPuzzle: function(data, callback) {
        let prev,
            first,
            sum = 0,
            puzzle = data.puzzle,
            size = puzzle.length,
            half = size / 2

        for (let i = 0; i < size; i++) {
            num = parseInt(puzzle[i])
            
            if (data.part === 1) {
                // Part One
                if (i === 0) {
                    first = num
                } else {
                    if (i === (size - 1) && num === first)
                        sum += num
                    if (prev === num)
                        sum += prev
                }
                prev = num
            } else {
                // Part Two
                if (i < half) {
                    if (num === parseInt(puzzle[i + half])) {
                        sum += num
                    }
                } else {
                    sum = sum * 2
                    break;
                }
            }
        }

        return callback(sum)
    },

    // Solve "Corruption Checksum"
    secondPuzzle: function(data, callback) {
        let arr = data.puzzle.split(' '),
            diffs = [],
            index = 0,
            sm = 0,
            lg = 0,
            result = 0

        arr.forEach((num) => {
            if (num === '+') {
                diffs[index] = lg - sm
                lg = 0, sm = 0
                index++
            } else {
                if (sm === 0 || parseInt(sm) > parseInt(num))
                    sm = parseInt(num)
                if (lg === 0 || parseInt(lg) < parseInt(num))
                    lg = parseInt(num)
            }
        })
        diffs[index] = lg - sm

        result = diffs.reduce((each, val) => {
            return each + val
        })

        return callback(result)
    },

    // Solve "High-Entropy Passphares"
    fourthPuzzle: function(data, callback) {
        let arr = data.puzzle.split(' + '),
            invalids = 0,
            length = arr.length

        for (let i = 0; i < arr.length; i++) {
            let subarr = arr[i].split(' '),
                invalid = false
            subarr.some(function (value, index, array) {
                if ((array.indexOf(value, index + 1) !== -1)) {
                    invalid = true
                }
            })
            if (invalid)
                invalids++
        }
        return callback(length - invalids)
    },
}

module.exports = Solver

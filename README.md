# adventofcode

> Advent of Code
>
> Solutions for the tasks listed on this page: https://adventofcode.com/2017

## Build Setup

``` bash
# install dependencies
npm install

# build for production and view the bundle analyzer report
npm start
```

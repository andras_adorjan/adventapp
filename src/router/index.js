import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/components/HomePage'
import DayComponent from '@/components/DayComponent'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/day/:id',
      name: 'DayComponent',
      component: DayComponent
    }
  ]
})
